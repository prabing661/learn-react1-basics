import React, { useState } from 'react'
import Prabin from './Prabin'
import LearnuseState from './LearnuseState'
import Counter from './component/Counter'
import HideAndSeek from './component/HideAndSeek'
import Toggle from './component/Toggle'
import HideComponent from './component/HideComponent'
import CheckArray from './CheckArray'
import LearnuseEffect from './LearnuseEffect'
import LearnuseEffect2 from './LearnuseEffect2'
import { render } from '@testing-library/react'
import LearnChildrenProps from './LearnChildrenProps'
import Form1 from './component/LearnForm/Form1'
import Form2 from './component/LearnForm/Form2'
import Form3 from './component/LearnForm/Form3'
import Form4 from './component/LearnForm/Form4'
import CreateContact from './component/LearnForm/CreateContact'
import HitApi2 from './component/LearnApi/HitApi2'
import ReadAllContact from './component/ReadAllContact'
import UseRef from './component/Learnuseref/UseRef'
import Routing from './component/LearnRouting/Routing'




const Nitan = () => {

  // let allBestFriends = ["me","Prabin","Bibash"]

  // let products = [
  //   {
  //     id: 1,
  //     title: "Product 1",
  //     category: "electronics",
  //     price: 5000,
  //     description: "This is description and Product 1",
  //     discount: {
  //       type: "other",
  //     },
  //   },
  //   {
  //     id: 2,
  //     title: "Product 2",
  //     category: "cloths",
  //     price: 2000,
  //     description: "This is description and Product 2",
  //     discount: {
  //       type: "other",
  //     },
  //   },
  //   {
  //     id: 3,
  //     title: "Product 3",
  //     category: "electronics",
  //     price: 3000,
  //     description: "This is description and Product 3",
  //     discount: {
  //       type: "other",
  //     },
  //   },
  // ]

  // let tagvariable = <p>this is another paragraph </p>

// Ternary operator
// let age = 23
// let canEnter = age>= 18?"You can enter" : "You cannot enter" //else part is most in ternary operator

// console.log(canEnter)
// let age = 20
// let massage = age===16?"Your age is 16": age===17?"Your age is 17" :age===18?"Your age is 18" : "Your age is neither 16,17,18"

// console.log(massage)

// let arr=true
// console.log([1,2,...arr?[1,3]:[4,6],4])

// let name = "Prabin"
// let obj= {
//   age : 23,
//   ...name?{name:name} : {}
// }
// console.log(obj)

 // return fun is also known as cleanup function 
//  Component did mount=1st render
//  Component did update=2nd  render
//  Component did unmount=whiel deletin the component, the return code will only execute

// let[showComponent,setShowComponent]=useState(true)






  return (
    <div>
      {/* <Prabin name = "Prabin" age = "23" address = "Lolang" phoneno = "9808471563"></Prabin> */}
      {/* <p style = {{ backgroundColor : "red" , color : "white" ,height : "100px" ,width : "200px" ,margin : "250px",padding :"150px",textAlign : "left"}}>this is paragraph </p>
      <button onClick = {()=>{
        console.log("button clicked")
    }}>CLICK ME</button>

    {
      allBestFriends.map((value,i)=>{
       
        return <p key = {i}>my bestfriend is {value}</p>
       
      })
    }

{
  products.map((value,i)=>{
    return <p key = {i}>{value.title} category is {value.category} it costs {value.price}</p>  
  })
}
 
 {tagvariable}

{
age===16?
(<p>Your age is 16</p>)
: age===17?
(<p>Your age is 17"</p>)
:age===18?
(<p>Your age is 18</p>) 
: (<p>Your age is neither 16,17,18</p>)
} */}

{/* <LearnuseState></LearnuseState> */}


{/* Renderring=
Component will render if state variable changes
if state variable doesn't change the component will not render */}

{/* <Counter></Counter>
<HideAndSeek></HideAndSeek>
<Toggle></Toggle>
<HideComponent></HideComponent>
 {/* Ctrl+shift+t = will reopen the old tab */}
{/* <CheckArray></CheckArray>  */}

 {/* <LearnuseEffect></LearnuseEffect> */}
   {/* {showComponent ?<LearnuseEffect2></LearnuseEffect2>  : null}  */}
  {/* {showComponent && <LearnuseEffect2></LearnuseEffect2> } 
  
  {/* When a component is removed ,Nothing execute except return code of useEffect   */}
   
   {/* <button
   onClick ={()=>{
   setShowComponent(false)
   }}
   >
    {""}
    hide useEffect</button>  */}
   

   
{/* 
   <hr />
   <LearnChildrenProps name = {"Prabin"} age ={23}>
    <div style = {{backgroundColor:"fuchsia"}}>
   <p>This is Children props</p>
   <h3>YOO</h3>
   </div>
   </LearnChildrenProps> */}

   {/* <Form1></Form1> */}
   {/* <Form2></Form2> */}
   {/* <Form3></Form3> */}
   {/* <Form4></Form4> */}
   {/* <CreateContact></CreateContact> */}
   {/* <HitApi1></HitApi1> */}
   {/* <HitApi2></HitApi2> */}
   {/* <ReadAllContact></ReadAllContact> */}
   {/* <UseRef></UseRef> */}
   <Routing></Routing>


   {/* <div className = "bg-red" >  
    <p>This Is Me</p>
    <p>Me is Prabin</p>
   </div>

   <div className = "abc" >  
    <p>This Is Me</p>
    <p>Me is Prabin</p>
   </div> */}

   
   
   





    </div>
  )
}

export default Nitan
