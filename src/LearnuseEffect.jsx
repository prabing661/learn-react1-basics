import React, { useEffect, useState } from 'react'

const LearnuseEffect = () => {
    let [count1,setCount1] = useState(0)
    let [count2,setCount2] = useState(100)
    let [count3,setCount3] = useState(200)
// when does useEffect function run?
// First render = the function runs
// Then afterwards it runs according dependency i.e[]
// In useEffect = in 1st render it doesnot execute
// First return fun execute then other code above return will execute
// From 2nd rendering it will run. 






// console.log("First First ")

// useEffect(() => {
// console.log("Prabin")

// return ()=>{
//   console.log("i am Sonbin")
// }
// },[count1,count2])

// useEffect(()=>{
// console.log("Sonbin")
// })

console.log("seconde")

  return (
    <div>
        <hr />
        <br />

      LearnuseEffect
      <br></br>
      count1 is {count1}
      <br></br>
      count2 is {count2}
      <br></br>
      <button 
      onClick={()=>{
     setCount1(count1 + 1)
      }}> Increase count1  </button>
      <br></br>
      <button 
      onClick={()=>{
     setCount2(count2 + 1)
      }}> Increase count2  </button>

    </div>
  )
}

export default LearnuseEffect
