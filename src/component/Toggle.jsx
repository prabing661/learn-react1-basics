import React, { useState } from 'react'

const Toggle = () => {
    let [show,setShow] = useState(true)
  return (
    <div>
        <br />
        {show? <p> Error has Occured </p> : null}
        <button 
        onClick = {()=>{
            setShow(!show)
        }}
        >  Toggle  </button>
      
    </div>
  )
}

export default Toggle
