import React, { useRef } from 'react'

const UseRef = () => {
    let inputRef1=useRef()
    let  inputRef2=useRef()
    let  inputRef3=useRef()
  return (
    <div>
      <p ref={inputRef1}>hello</p>
      <br></br>
      <p ref={inputRef2}>hey</p>
      <br></br>
      <button
      onClick={() =>{
        inputRef1.current.style.backgroundColor="red"
        inputRef2.current.style.backgroundColor="yellow"
      }}
      >Change BackGround</button>
    <br></br>
    <br></br>
    <br></br>

    <input ref={inputRef3}></input>
<br></br>

<button
onClick={()=>{
    inputRef3.current.focus()
}}
>Focus</button>

  </div>
  )
}

export default UseRef
