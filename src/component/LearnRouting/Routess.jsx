import React from 'react'
import { Route, Routes } from 'react-router-dom'
import Home from './Home'
import Contact from './Contact'
import About from './About'
import LearnToGetDynamicRouteValue from './LearnToGetDynamicRouteValue'
import LearnToGetSearchParams from './LearnToGetSearchParams'

const Routess = () => {
  return (
    <div>
      <Routes>
         <Route path= "/" element ={<Home></Home>}></Route>
         <Route path= "/contact" element ={<Contact></Contact>}></Route>
         <Route path= "/about" element ={<About></About>}></Route>
         <Route path= "/about/1" element ={<div>Wanna know About us?1</div>}></Route>
         <Route path= "/about/:a" element ={<div>common about</div>}></Route>

         <Route path= "/contact/:id/id1/:id2" 
         element ={<LearnToGetDynamicRouteValue></LearnToGetDynamicRouteValue>}>
         </Route>
         <Route path= "/product" 
         element ={<LearnToGetSearchParams></LearnToGetSearchParams>}>
         </Route>
         

      </Routes>
      
    </div>
  )
}

export default Routess
