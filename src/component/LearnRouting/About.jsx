import React from 'react'
import { useNavigate } from 'react-router-dom'

const About = () => {
  let navigate = useNavigate()
  return (
    <div>
      About Us
      <button 
      onClick = {()=>{
         navigate("/contact",{replace:true})
      }}
      >Go to Contact</button>
    </div>
  )
}

export default About
