import React from 'react'
import { useParams } from 'react-router-dom'

const LearnToGetDynamicRouteValue = () => {
  let params=useParams()

  console.log(params)
  return (
    <div>
      Dynamic Routes
    </div>
  )
}

export default LearnToGetDynamicRouteValue
