import React from 'react'
import { useSearchParams } from 'react-router-dom'

const LearnToGetSearchParams = () => {
  let [params]=useSearchParams()

  let name = params.get('name')

  let age = params.get('age')


  return (
    <div>
      name is {name}
      <br></br>
      age is {age}
      <br></br>
      Search Params
    </div>
  )
}

export default LearnToGetSearchParams
