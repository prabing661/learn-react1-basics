import React, { useState } from 'react'

function Form4() {
  let [productName,setProductName]=useState("")
	let [productManagerEmail,setProductManagerEmail] = useState("")
	let [staffEmail,setStaffEmail] = useState("")
	let [password,setPassword] = useState("")
	let [gender,setGender] = useState("")
	let [productManufactorDate,setProductManufactorDate] = useState("")
	let [managerIsMarried,setManagerIsMarried]=useState(true)
	let [managerSpouse,setManagerSpouse]=useState("")
	let [productLocation,setProductLocation]=useState("")
	let [productDescription,setProductDescription]=useState("")
	let [productIsAvailable,setProductIsAvailable]=useState(false)

  let spouses=[
    {label:"Husband",value:"husband"},
    {label:"Wife",value:"wife"},
  ]

  let genders=[
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];
  return (
    <form
    onSubmit={(e)=>{
      e.preventDefault()
      let details={
  productName,
	productManagerEmail,
	staffEmail,
	password,
	gender,
	productManufactorDate,
	managerIsMarried,
	managerSpouse,
	productLocation,
	productDescription,
	productIsAvailable,
      }
      console.log(details)
    }}
    >
      <br></br>
      <label htmlFor="productName">Product Name: </label>
       <input
       id="productName"
       type="text"
       value={productName}
       onChange={(e)=>{
        setProductName(e.target.value)
       }}
      ></input>

      <br></br>
      <label htmlFor="productManagerEmail">Product Manager Email: </label>
       <input
       id="productManagerEmail"
       type="email"
       value={productManagerEmail}
       onChange={(e)=>{
        setProductManagerEmail(e.target.value)
       }}
      ></input>

      <br></br>
      <label htmlFor="staffEmail">Staff Email: </label>
       <input
       id="staffEmail"
       type="email"
       value={staffEmail}
       onChange={(e)=>{
        setStaffEmail(e.target.value)
       }}
      ></input>

      <br></br>
      <label htmlFor="password">Password: </label>
       <input
       id="password"
       type="password"
       value={password}
       onChange={(e)=>{
        setPassword(e.target.value)
       }}
      ></input>

      <br></br>
      <label htmlFor="gender">Gender: </label>
      {
        genders.map((item,i)=>{
            return(
                <>
                <label htmlFor={item.value}>{item.value}</label>
                <input
                checked={gender===item.value}
                id ={item.value}
                type="radio"
                value={item.value}
                onChange={(e)=>{
                    setGender(e.target.value)
                }}
                ></input>
                </>
            )
        })
     }

      <br></br>
      <label htmlFor="productManufactorDate">Product Maufactor Date: </label>
       <input
       id="productManufactorDate"
       type="date"
       value={productManufactorDate}
       onChange={(e)=>{
        setProductManufactorDate(e.target.value)
       }}
      ></input>

      <br></br>
      <label htmlFor="managerIsMarried">Manager IsMarried: </label>
       <input
       id="managerIsMarried"
       type="checkbox"
       value={managerIsMarried}
       onChange={(e)=>{
        setManagerIsMarried(e.target.checked)
       }}
      ></input>

<br></br>
      <label htmlFor="managerSpouse">Manager Spouse: </label>
      {
        spouses.map((item,i)=>{
            return(
                <>
                <label htmlFor={item.value}>{item.value}</label>
                <input
                checked={gender===item.value}
                id ={item.value}
                type="radio"
                value={item.value}
                onChange={(e)=>{
                    setManagerSpouse(e.target.value)
                }}
                ></input>
                </>
            )
        })
     }

      <br></br>
      <button type="Submit">Send</button>
    </form>
  )
}

export default Form4
