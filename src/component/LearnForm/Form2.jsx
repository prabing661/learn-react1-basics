import React, { useState } from 'react'

function Form2() {
    let [name,setName]=useState("")
    let [address,setAddress]=useState("")
    let [password,setPassword]=useState("")
    let [description,setDescription]=useState("")
    let [isMarried,setIsMarried]=useState(false)
    let [country,setCountry]=useState("")
    let [favmovie,setFavmovie]=useState("")
    let[gender,setGender]=useState("")    
    let countries = [
      { label: "Select Country", value: "", disabled: true },
      { label: "Nepal", value: "nepal" },
      { label: "China", value: "china" },
      { label: "India", value: "india" },
      { label: "America", value: "america" },
    ];
   let favmovies=[
    { label : "Select movies",value :"" , disabled: true },
    { label : "Undisputed",value : "uni" },
    { label : "Paranoah" , value : "para"},
    { label : "Thritythieves", value : "thiref"},
    { label : "NepalwarZone",value:"NWZ"},
   ]
   let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

   
  return<form
  onSubmit={(e)=>{
  e.preventDefault()
  let info={
     name,
     address,
     password,
     description,
     isMarried,
     country,
     favmovie,
     gender,
  }
  console.log(info)
}}
  >
    <br></br>
<label htmlFor = "name">Name: </label>
    <input 
    id="name"
    type="text" 
    placeholder = "Enter Your Name"
    value={name}
    onChange={(e)=>{
        setName(e.target.value)
    }}></input>

    <br></br>

<label htmlFor = "address">Address: </label>
    <input
    id="address"
    type = "text" 
    placeholder = "Enter Your Address"
    value={address}
    onChange={(e)=>{
        setAddress(e.target.value)
    }}></input>
    <br></br>

    <label htmlFor="password">Password: </label>
     <input 
     id = "password"
     type = "password"
     placeholder="Enter Your Password"
     value={password}
     onChange={(e)=>{
      setPassword(e.target.value)
     }}
     ></input>

     <br></br>
    <label htmlFor="description">Description: </label>
     <textarea 
     id = "description"
     placeholder="Enter The Description"
     value={description}
     onChange={(e)=>{
      setDescription(e.target.value)
     }}
     rows={10}
     cols={15}
     >
     </textarea>
     <br></br>
    <label htmlFor="isMarried">IsMarried: </label>
     <input
     id = "isMarried"
     type ="checkbox"
     checked={isMarried}//all uses value but boxes use checked 
     onChange={(e)=>{
      setIsMarried(e.target.checked) // all uses e.target.value but only Checkbox uses e.target.checked
     }}

     ></input>

     <br></br>

    <label htmlFor = "country">Country: </label>
    {/* <select id = "country"
    value = {country}
    onChange={(e)=>{
      setCountry(e.target.value)
    }}
    >
      <option value = "" disabled ={true}>Select Country</option>
      <option value = "nep" >Nepal</option>
      <option value = "chi" >China</option>
      <option value = "ind" >India</option>
      <option value = "usa" >America</option>

     </select> */}

    
    
  <select name ="" 
  id = "countries"
  value = {country}
  onChange={(e)=>{
    setCountry(e.target.value)
  }}
  >
    {
      countries.map((item,i)=>{
        return(<option value={item.value} disabled = {item.disabled}>{item.label}</option>)
      })
    } </select>
   
  <br></br>

<label htmlFor = "favmovie">FavMovies: </label>

 <select name = ""
 id = "favmovie"
 value={favmovie}
 onChange={(e)=>{
  setFavmovie(e.target.value)
 }}
 >
  {
    favmovies.map((item,i)=>{
      return(<option key = {i} value={item.value} disabled = {item.disabled}>{item.label}</option>)
    })
    }
 </select>


<br></br>
<br></br>
<br></br>

{/* <label htmlFor ="">Gender: </label>

<label htmlFor = "male">Male</label>
<input 
checked = {gender==="male"}
onChange={(e)=>{
  setGender(e.target.value)
}}
 type= "radio" 
 id = "male" 
 value = "male"
 ></input>

<label htmlFor = "female">Female</label>
<input 
checked ={gender==="female"}
onChange={(e)=>{
   setGender(e.target.value)
}}
type= "radio" 
id = "female" 
value = "female"
></input>

<label htmlFor = "other">Other</label>
<input 
checked = {gender==="other"}
onChange={(e)=>{
  setGender(e.target.value)
}}
type= "radio" 
id = "other" 
value = "other"
></input> */}

<br></br>

<label htmlFor ="">Gender: </label>
{
  genders.map((item,i)=>{
    return(
      <>
      <label htmlFor={item.value} >{item.label}</label> 
      <input
      checked ={gender===item.value}
      onChange={(e)=>{
        setGender(e.target.value)
      }}
      type = "radio"
      id={item.value}
      value={item.value}

      ></input>
      </>

    )
  })
}









<br></br>




    <button type ="sumbit">Send</button>
  </form>
}

export default Form2
