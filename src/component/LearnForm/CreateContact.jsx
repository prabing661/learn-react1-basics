import axios from 'axios'
import React, { useState } from 'react'
import { baseUrl } from '../../config/config'

function CreateContact() {
    let [fullName,setFullName]=useState("Prabin")
    let [address,setAddress]=useState("")
    let [phoneNumber,setPhoneNumber]=useState("")
    let [email,setEmail]=useState("")

  return (
    <form
    onSubmit={async (e)=>{
     e.preventDefault()

     let data = {
    fullName,
    address,
    phoneNumber,
    email,
     }
     let info ={
        url:`${baseUrl}/contacts`,
        method:"post",
        data,
     }
    
     try{
        let result = await axios(info)
        console.log(result.data.message)
     }catch(error){
        // console.log(error.message)
     }


    }}
    >
<br></br>
<label htmlFor = "fullName">FullName: </label>
<input
id = "fullName"
type="text"
value={fullName}
onChange={(e)=>{
    setFullName(e.target.value)
}}
></input>

<br></br>
<label htmlFor = "address">Address: </label>
<input
id = "address"
type="text"
value={address}
onChange={(e)=>{
    setAddress(e.target.value)
}}
></input>

<br></br>
<label htmlFor = "phoneNumber">Phone Number: </label>
<input
id = "phoneNumber"
type="number"
value={phoneNumber}
onChange={(e)=>{
    setPhoneNumber(e.target.value)
}}
></input>

<br></br>
<label htmlFor = "email">Email: </label>
<input
id = "email"
type="email"
value={email}
onChange={(e)=>{
    setEmail(e.target.value)
}}
></input>



<br></br>
        <button type="submit">Send</button>
    </form>
  )
}

export default CreateContact
