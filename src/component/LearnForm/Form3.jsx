import React, { useState } from 'react'

// make  form with field name,age,emil,gender

function Form3() {
    let [name,setName]=useState("")
    let [age,setAge]=useState("")
    let [email,setEmail]=useState("")
    let [gender,setGender]=useState("")

    let genders=[
        {label: "Male", value: "male" },
        { label: "Female", value: "female" },
        { label: "Other", value: "other" },
      ];


return (
    <form 
    onSubmit = {(e)=>{
       e.preventDefault()
       let bio={
        name,
        age,
        email,
        gender,
       }
       console.log(bio)
    }}
    >
        <br></br>
    <label htmlFor="name">Name: </label>
     <input
     id = "name"
     type="text"
     placeholder = "Enter name"
     value={name}
     onChange={(e)=>{
        setName(e.target.value)
     }} 
     ></input>

        <br></br>

    <label htmlFor="age">Age: </label>
     <input
     id = "age"
     type="number"
     placeholder = "Enter age"
     value={age}
     onChange={(e)=>{
        setAge(e.target.value)
     }} 
     ></input>

        <br></br>

    <label htmlFor="email">Email: </label>
     <input
     id = "email"
     type="email"
     value={email}
     onChange={(e)=>{
        setEmail(e.target.value)
     }} 
     ></input>

        <br></br>

    <label htmlFor="gender">Gender: </label>
     {
        genders.map((item,i)=>{
            return(
                <>
                <label htmlFor={item.value}>{item.value}</label>
                <input
                checked={gender===item.value}
                id ={item.value}
                type="radio"
                value={item.value}
                onChange={(e)=>{
                    setGender(e.target.value)
                }}
                ></input>
                </>
            )
        })
     }





        <br></br>
        <button type="submit">Send</button>
    </form>
  )
}

export default Form3
