import React, { useState } from 'react'

const HideAndSeek = () => {
    let [show , setShow] = useState(true)
  return (
    <div>
        <br />
        <hr />
        {show? <p>Error has Occured</p> : null}
      <button 
      onClick = {()=>{
        setShow(false)
      }}>Hide</button>
      <br></br>
      <button
      onClick = {()=>{
        setShow(true)
      }}>Show</button>
    </div>
  )
}

export default HideAndSeek
