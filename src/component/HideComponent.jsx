import React, { useState } from 'react'
import ShowComponent from './ShowComponent'

const HideComponent = () => {
  let [show,setshow] = useState(true)
  return (
    <div>
      {show? <ShowComponent></ShowComponent>  : null}
      <button 
      onClick = {()=> {
       setshow(!show)
      }}>Click To Show </button>
    </div>
  )
}

export default HideComponent
