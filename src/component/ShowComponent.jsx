import React from 'react'

const ShowComponent = () => {
  return (
    <div>
      <p>this component should be shown</p>
    </div>
  )
}

export default ShowComponent
