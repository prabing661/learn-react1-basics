import axios from 'axios'
import React from 'react'

const HitApi2 = () => {
    let readAllContacts= async () => {
        let obj = {
            url:"https://fake-api-nkzv.onrender.com/api/v1/contacts",
            method:"get",
        }
        let result = await axios(obj)
        
        console.log(result.data.data.results)
        
    }

    let createContacts = async ()=>{
      let obj1 ={
        url:"https://fake-api-nkzv.onrender.com/api/v1/contacts",
        method:"post",
        data:{
          fullName:"Prabin",
          address:"gagalphedi",
          phoneNumber:9849468999,
          email:"nitanthapa425@gmail.com"
        },
      }
      let result1= await axios(obj1)

      console.log(result1)
    }
  return (
    <div>
      <button 
      onClick={()=>{
       readAllContacts()
      }}>Read all Contacts</button>

      <br></br>
      <button onClick={()=>{
        createContacts()
      }}>CreateContacts</button>


    </div>
  )
}

export default HitApi2
