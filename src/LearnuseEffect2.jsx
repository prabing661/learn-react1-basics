import React, { useEffect, useState } from 'react'

const LearnuseEffect2 = () => {
    let [count,setCount]=useState(0)

    useEffect(()=>{
        console.log("i am asychronous effect")

        return ()=>{
            console.log("i am return")
        }
    },[count])

    console.log("i am Secondee")
      return (
    <div style = {{border: "solid maroon 6px"}}>
      {count}
      <br></br>
      <button 
      onClick ={()=>{
        setCount(count + 1)
      }}
      >Increase Count</button>
    </div>
  )
}

export default LearnuseEffect2
