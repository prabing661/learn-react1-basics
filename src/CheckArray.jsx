import React, { useState } from 'react'

// Asychronous = it sends the code the memory code and then runs at last
const CheckArray = () => {
    let [arr1,setArr1]=useState(1)
  return (
    <div>
      {arr1}
      <br></br>
      <button
      onClick={()=>{
        setArr1(2)
        console.log(arr1)
        setArr1(3)
        console.log(arr1)
        setArr1(4)
      }}>
        change</button>
    </div>
  )
}

export default CheckArray
